'use strict';

/* Controllers */

angular.module('TestAngularJS.controllers', [])
        .controller('ListaPelisCtrl', ['$scope', 'rtmFactory',
            function($scope, rtmFactory) {
                $scope.countries = rtmFactory.getCountries();
                $scope.loadMovies = function(/*countriesCode*/) {
                    rtmFactory.getMovies(/*countriesCode*/).then(
                            function(response) {
                                var movieArray = response.data.robots;
                                $scope.movies = movieArray;
                            },
                            function(response) {
                                //error message
                                console.log("ERROR " + response.status + response.data);
                            }
                    )
                };
                $scope.loadMoviesType = function(type) {
                    rtmFactory.getMovies(type).then(
                            function(response) {
                                var movieArray = response.data.robots;
                                console.log(JSON.stringify(response.data));
                                $scope.movies = movieArray;
                            },
                            function(response) {
                                //error message
                                console.log("ERROR " + response.status + response.data);
                            }
                    )
                };
                //$scope.loadMovies('us');
                $scope.loadMovies();
            }
        ]);

/*
 angular.module('TestAngularJS.controllers', [])
 .controller('AlumnosController',
 function($scope) {
 $scope.alumnos = [
 {"name": "pasilla", "spiciness": "mild"},
 {"name": "jalapeno", "spiciness": "hot hot hot!"},
 {"name": "habanero", "spiciness": "LAVA HOT!!"}
 ];
 $scope.alumnos = "habanero";
 });
 */

/*
 .controller('MyCtrl1', ['$scope', function($scope) {
 }])
 .controller('MyCtrl2', ['$scope', function($scope) {
 }]);
 */
