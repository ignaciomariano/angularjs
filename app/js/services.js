'use strict';
        /* Services */


// Demonstrate how to register services
// In this case it is a simple value service.
        angular.module('TestAngularJS.services', []).
        value('version', '0.1')
        //.constant('API_KEY', 'esto_nunca_va_a_funcionar_con_api_key')
        .constant('API_KEY', 'b7444u35pevm9nw3bxwstbxt')

        .factory('rtmFactory', ['$http', 'API_KEY', function($http, API_KEY) {
        var countries = [
        {name: 'US', code: 'us'},
        {name: 'RU', code: 'uk'},
        {name: 'Francia', code: 'fr'},
        ];
        return{
                getCountries: function() {
                        return countries;
                },
                getMovies: function() {
                        return $http.jsonp('http://api.rottentomatoes.com/api/public/v1.0/movies/770672122.json?apikey=' + API_KEY);
                //return $http.get('http://api-rest/robots/');
                },
                getMoviesType: function(typr) {
                        return $http.jsonp('http://api.rottentomatoes.com/api/public/v1.0/movies/770672122.json?apikey=' + API_KEY);
                        //return $http.get('http://api-rest/robots/');
                }

        }
}]);