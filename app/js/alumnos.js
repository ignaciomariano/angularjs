



function AlumnosController($scope) {
    $scope.alumnos = [
        {nombre: 'Fran Marasco', telefono: '15-1111-1111', curso: 'Primero A'},
        {nombre: 'Martin Buroni', telefono: '15-2222-2222', curso: 'Primero B'},
        {nombre: 'Nachito Gonzalez', telefono: '15-3333-3333', curso: 'Primero C'},
        {nombre: 'Leandro Wiit', telefono: '15-4444-4444', curso: 'Primero D'}
    ];

    $scope.Save = function() {
        $scope.alumnos.push({
            nombre: $scope.nuevoAlumno.nombre,
            telefono: $scope.nuevoAlumno.telefono,
            curso: $scope.nuevoAlumno.curso,
        });
        $scope.FormVisibility=false;
    }
    
    $scope.FormVisibility=false;
    
    $scope.ShowForm=function(){
        $scope.FormVisibility=true;
    }
}

/*
 var myApp = angular.module('myApp', []);
 
 myApp.controller('AlumnosController', function($scope) {
 $scope.alumnos = [
 {"name": "pasilla", "spiciness": "mild"},
 {"name": "jalapeno", "spiciness": "hot hot hot!"},
 {"name": "habanero", "spiciness": "LAVA HOT!!"}
 ];
 $scope.alumnos = "habanero";
 });
 */