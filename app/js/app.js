'use strict';


// Declare app level module which depends on filters, and services
angular.module('TestAngularJS', [
    'ngRoute',
    'TestAngularJS.filters',
    'TestAngularJS.services',
    'TestAngularJS.directives',
    'TestAngularJS.controllers'
]).
        config(['$routeProvider', function($routeProvider) {
                $routeProvider.when('/', {templateUrl: 'partials/lista-pelis.html', controller: 'ListaPelisCtrl'});
                $routeProvider.otherwise({redirectTo: '/'});


                //$routeProvider.when('/test', {templateUrl: 'partials/test.html', controller: 'AlumnosController'});
                //$routeProvider.otherwise({redirectTo: '/'});

                /*
                 $routeProvider.when('/view1', {templateUrl: 'partials/partial1.html', controller: 'MyCtrl1'});
                 $routeProvider.when('/view2', {templateUrl: 'partials/partial2.html', controller: 'MyCtrl2'});
                 $routeProvider.otherwise({redirectTo: '/view1'});
                 */
            }]);
